# TreinamentoMiddleware ObjectEdge
written by: Matheus Anzzulin

How to start the TreinamentoMiddleware application 

Prerequisites: MySQL database, Java 8

---

1. Run `mvn clean package` to build this application
2. Migate db with `java -jar target/TreinamentoMiddleware-0.0.1-SNAPSHOT.jar db migrate config.yml`
3. Start application with `java -jar target/TreinamentoMiddleware-0.0.1-SNAPSHOT.jar server config.yml`
4. To check the short list enter url `http://localhost:8080/Users/Short`
5. To check the complete list enter url `http://localhost:8080/Users/Complete`

---
No Health Checks were made