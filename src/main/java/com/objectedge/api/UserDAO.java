package com.objectedge.api;

import com.objectedge.core.User;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class UserDAO extends AbstractDAO<User> {
    public UserDAO(SessionFactory factory) {
        super(factory);
    }

    public long create(User user) {
        return persist(user).getId();
    }

    public List<User> listAll() {
        return list(namedQuery("listAll"));
    }

    public List<User> showAll() {
        return list(namedQuery("showAll"));
    }

}
