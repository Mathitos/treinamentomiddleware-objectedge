package com.objectedge.resources;


import com.codahale.metrics.annotation.Timed;
import com.objectedge.api.UserDAO;
import com.objectedge.core.User;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@Path("/Users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private UserDAO userDAO;

    public UserResource(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    @Path("/Short")
    @GET
    @UnitOfWork
    public List<User> listAll() {
        return userDAO.listAll();
    }
    @Path("/Complete")
    @GET
    @UnitOfWork
    public List<User> showAll() {
        return userDAO.showAll();
    }


}
