package com.objectedge.core;


import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = "listAll",
                query = "SELECT e.id,e.nome FROM User e"),
        @NamedQuery(name = "showAll",
                query = "SELECT e FROM User e"),

})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "data_de_criacao")
    private String data_de_criacao;

    @Column(name = "status")
    private String status;

    @Column(name = "data_de_nascimento")
    private String data_de_nascimento;


    public User() {
    }

    public User(long id,String nome, String data_de_criacao, String status, String data_de_nascimento) {
        this.id = id;
        this.nome = nome;
        this.data_de_criacao = data_de_criacao;
        this.status = status;
        this.data_de_nascimento = data_de_nascimento;
    }

    public long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getData_de_criacao() {
        return data_de_criacao;
    }

    public String getStatus() {
        return status;
    }

    public String getData_de_nascimento() {
        return data_de_nascimento;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setData_de_criacao(String data_de_criacao) {
        this.data_de_criacao = data_de_criacao;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setData_de_nascimento(String data_de_nascimento) {
        this.data_de_nascimento = data_de_nascimento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() &&
                Objects.equals(getNome(), user.getNome()) &&
                Objects.equals(getData_de_criacao(), user.getData_de_criacao()) &&
                Objects.equals(getStatus(), user.getStatus()) &&
                Objects.equals(getData_de_nascimento(), user.getData_de_nascimento());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getNome(), getData_de_criacao(), getStatus(), getData_de_nascimento());
    }
}
