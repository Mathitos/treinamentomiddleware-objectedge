package com.objectedge;


import com.objectedge.api.UserDAO;
import com.objectedge.core.User;
import com.objectedge.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

import java.sql.Connection;


public class TreinamentoMiddlewareApplication extends Application<TreinamentoMiddlewareConfiguration> {

    public static void main(final String[] args) throws Exception {
        new TreinamentoMiddlewareApplication().run(args);
    }

    @Override
    public String getName() {
        return "TreinamentoMiddleware";
    }

    /*
     *Hibernate Bundle
     */
    private final HibernateBundle<TreinamentoMiddlewareConfiguration> hibernate = new HibernateBundle<TreinamentoMiddlewareConfiguration>(User.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(TreinamentoMiddlewareConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };


    @Override
    public void initialize(Bootstrap<TreinamentoMiddlewareConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(new MigrationsBundle<TreinamentoMiddlewareConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(TreinamentoMiddlewareConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final TreinamentoMiddlewareConfiguration config,
                    final Environment environment) {
        final UserDAO dao = new UserDAO(hibernate.getSessionFactory());
        environment.jersey().register(new UserResource(dao));
    }

}

